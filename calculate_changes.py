import subprocess

def calculate_change_percentage(target_branch, commit_ref):
    try:
        # Get the diff between the source and target branches for README.md
        diff_output = subprocess.check_output(['git', 'diff', f'{target_branch}..{commit_ref}', '--', 'README.md'], text=True)
        
        # Calculate the total number of lines changed
        total_lines_changed = len(diff_output.splitlines())
        
        # Calculate the total number of lines in the file
        with open('README.md', encoding='utf-8') as file:
            total_lines_in_file = len(file.readlines())
        
        # Calculate the percentage of changes
        if total_lines_in_file == 0:
            change_percentage = 100  # If the file is empty, consider it as 100% change
        else:
            change_percentage = (total_lines_changed / total_lines_in_file) * 100
            
        return change_percentage
        
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")
        exit(1)

if __name__ == "__main__":
    target_branch = 'origin/main'
    commit_ref = 'remotes/origin/feature/nani'
    
    try:
        percentage = calculate_change_percentage(target_branch, commit_ref)
        print(f"Percentage of changes in README.md between '{target_branch}' and '{commit_ref}': {percentage:.2f}%")
        
        # Check if the percentage of changes exceeds 5%
        if percentage > 5:
            print("Changes exceed 5%. Preventing merge request creation.")
            exit(1)
        else:
            print("Changes within threshold. Proceeding with merge request creation.")
            # Add code to create the merge request here
            
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")
        exit(1)
